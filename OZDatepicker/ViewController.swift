//
//  ViewController.swift
//  OZDatepicker
//
//  Created by George Termentzoglou on 06/01/2019.
//  Copyright © 2019 OneZero. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func showDate(_ sender: Any) {
        OZDatepicker.present(inView: self.view,){ date in
            print(date)
        }
    }
    override func viewWillLayoutSubviews() {
        
    }

}

