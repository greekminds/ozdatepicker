//
//  OZDatepicker.swift
//  OZDatepicker
//
//  Created by George Termentzoglou on 06/01/2019.
//  Copyright © 2019 OneZero. All rights reserved.
//

import UIKit

class OZDatepicker: UIView {
    
    typealias SelectionCallback = (Date)->Void
    
    private var stackView:ParentStackView = ParentStackView()
    private var transparentView = UIView(frame:.zero)
    private var selectionHandler:SelectionCallback?
    private var stackTempConstraint:NSLayoutConstraint?
    private var stackBottomConstraint:NSLayoutConstraint?
    
    /// Calls bar with "Hello, world"
    /// - parameter parentView: the view in which the datepicker is embedded
    /// - parameter initDate: (withDate:) -Optional- Initial date displayed to the user
    /// - parameter buttonColor: (confirmButtonColor:) -Optional- the color of the button user presses to choose the date
    /// - parameter onDateSelection: the closure returned to parent view with the selected date
    
    static func present(inView parentView:UIView,withDate initDate:Date? = nil,confirmButtonColor buttonColor:UIColor? = nil,onDateSelection: @escaping SelectionCallback){
        
        let datepicker = OZDatepicker(onDateSelection:onDateSelection)
        datepicker.stackView.confirmButtonView.button.backgroundColor = buttonColor ?? #colorLiteral(red: 1, green: 0.4647622592, blue: 0.3395120558, alpha: 1)
        datepicker.stackView.datePicker.date = initDate ?? Date()
        
        parentView.addSubview(datepicker)
        NSLayoutConstraint.activate([
            datepicker.topAnchor.constraint(equalTo: parentView.topAnchor),
            datepicker.bottomAnchor.constraint(equalTo: parentView.bottomAnchor),
            datepicker.leadingAnchor.constraint(equalTo: parentView.leadingAnchor),
            datepicker.trailingAnchor.constraint(equalTo: parentView.trailingAnchor)
            ])
        
        datepicker.layoutIfNeeded()
        datepicker.animate(show:true)
    }
    
    deinit {
        print("OZDatepicker released. No strong references")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(onDateSelection:@escaping SelectionCallback){
        super.init(frame: .zero)
        setup()
        selectionHandler = onDateSelection
    }
    
    private func setup(){
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = UIColor.clear
        
        self.addTransparentBackground()
        self.addStackView()
        
        self.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(panDetected(gesture:))))
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapDetected(gesture:))))
    }
    
    
    private func animate(show:Bool){
        if(show){
            stackTempConstraint!.isActive = false
            UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseInOut, animations: {
                [unowned self] in
                self.layoutIfNeeded()
                self.transparentView.alpha = 0.5
            }) { (completed) in}
        }
        else{
            stackTempConstraint!.isActive = true
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                [unowned self] in
                self.layoutIfNeeded()
                self.transparentView.alpha = 0.0
            }) { (completed) in
                self.removeFromSuperview()
            }
        }
        
    }
    
    private func addStackView(){
        stackView = ParentStackView()
        stackView.axis  = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(stackView)
        
        stackTempConstraint = stackView.topAnchor.constraint(equalTo: self.bottomAnchor)
        stackBottomConstraint = stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: UIDevice.current.userInterfaceIdiom == .pad ? -30:0)
        stackBottomConstraint!.priority = UILayoutPriority(750)
        
        NSLayoutConstraint.activate([
            stackTempConstraint!,
            stackBottomConstraint!,
            stackView.centerXAnchor.constraint(equalTo: self.centerXAnchor)
            ])
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            NSLayoutConstraint.activate([
                stackView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6)
                ])
        }
        else{
            NSLayoutConstraint.activate([
                stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                ])
        }
        
        self.addDatepicker()
        self.addConfirmButton()
    }
    
    private func addTransparentBackground(){
        
        transparentView = UIView(frame:.zero)
        transparentView.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        transparentView.translatesAutoresizingMaskIntoConstraints = false
        transparentView.alpha = 0
        self.addSubview(transparentView)
        
        NSLayoutConstraint.activate([
            transparentView.topAnchor.constraint(equalTo: self.topAnchor),
            transparentView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            transparentView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            transparentView.trailingAnchor.constraint(equalTo: self.trailingAnchor)
            ])
        
    }
    
    private func addDatepicker(){
        
        //self constrains to superview
        
        let datepicker = stackView.datePicker
        datepicker.translatesAutoresizingMaskIntoConstraints = false
        datepicker.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        //datepicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)
        
        datepicker.clipsToBounds = true
        datepicker.layer.cornerRadius = UIDevice.current.userInterfaceIdiom == .pad ? 40:20
        datepicker.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        stackView.addArrangedSubview(datepicker)
    }
    
    private func addConfirmButton(){
        let confirmButtonView = stackView.confirmButtonView
        confirmButtonView.translatesAutoresizingMaskIntoConstraints = false
        confirmButtonView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            confirmButtonView.layer.cornerRadius = 40
            confirmButtonView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        }
        
        stackView.addArrangedSubview(confirmButtonView)
        
        //confirmButtonView constrains
        NSLayoutConstraint.activate([
            confirmButtonView.heightAnchor.constraint(greaterThanOrEqualToConstant: 90)
            ])
        
        let confirmButton = confirmButtonView.button
        confirmButton.translatesAutoresizingMaskIntoConstraints = false
        confirmButton.addTarget(self, action: #selector(confirmButtonClicked), for: .touchUpInside)
        confirmButton.addTarget(self, action: #selector(confirmButtonDown), for: .touchDown)
        confirmButton.addTarget(self, action: #selector(confirmButtonCancel), for: .touchCancel)
        confirmButton.clipsToBounds = true
        confirmButton.layer.cornerRadius = 27
        confirmButton.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner,.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        confirmButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for:.normal)
        confirmButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        confirmButton.setTitle("Confirm", for: .normal)
        
        confirmButton.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: 2.0, opacity: 0.15)
        
        confirmButtonView.addSubview(confirmButton)
        
        //confirmButton constrains
        let widthConst = confirmButton.widthAnchor.constraint(equalTo: confirmButtonView.widthAnchor, multiplier: 0.8)
        widthConst.priority = UILayoutPriority(750)
        
        NSLayoutConstraint.activate([
            confirmButton.heightAnchor.constraint(equalToConstant:57),
            widthConst,
            confirmButton.widthAnchor.constraint(lessThanOrEqualToConstant: 300),//mainly for iPads
            confirmButton.centerXAnchor.constraint(equalTo: confirmButtonView.centerXAnchor),
            confirmButton.centerYAnchor.constraint(equalTo: confirmButtonView.centerYAnchor, constant: -5)
            ])
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

// MARK: - Interactions
    private static var initialPoint:CGPoint? = nil
    @objc func panDetected(gesture: UIPanGestureRecognizer) {
    
        let translatedPoint:CGPoint = gesture.translation(in: self)
        
        switch gesture.state {
        case .began:
            OZDatepicker.initialPoint = translatedPoint
        case .changed:
            let distanceInPixels = translatedPoint.y - OZDatepicker.initialPoint!.y
            if(distanceInPixels<0){return} //exceeds initial position
            else if(distanceInPixels>stackView.frame.size.height/2.0){
                if(gesture.isEnabled){ //just in case,to avoid called multiple times
                    gesture.isEnabled = false
                    animate(show: false)
                }
            }
            else{
                stackBottomConstraint?.constant = distanceInPixels
                let alphaMultiplier = distanceInPixels/stackView.frame.size.height
                self.transparentView.alpha = 0.5 - (alphaMultiplier/2)
                self.layoutIfNeeded()
            }
        case .ended, .possible, .cancelled, .failed:
            let distanceInPixels = translatedPoint.y - OZDatepicker.initialPoint!.y
            if(distanceInPixels<stackView.frame.size.height/2.0){
                stackBottomConstraint?.constant = UIDevice.current.userInterfaceIdiom == .pad ? -30:0
                UIView.animate(withDuration: 0.3) {
                    self.transparentView.alpha = 0.5
                    self.layoutIfNeeded()
                }
            }
        }
        
    }
    
    @objc func tapDetected(gesture: UITapGestureRecognizer) {
        gesture.isEnabled = false
        animate(show: false)
    }
    
    
//    @objc func datePickerChanged(picker: UIDatePicker) {
//        let date = picker.date
//    }
    
    @objc func confirmButtonDown(sender : UIButton){
        UIView.animate(withDuration: 0.1) {
            sender.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
        }
    }
    
    @objc func confirmButtonCancel(sender : UIButton){
        UIView.animate(withDuration: 0.1) {
            sender.transform = CGAffineTransform.identity
        }
    }
    
    @objc func confirmButtonClicked(sender : UIButton){
        
        UIView.animate(withDuration: 0.1) {
            sender.transform = CGAffineTransform.identity
        }
        
        if let selectionHandler = selectionHandler{
            selectionHandler(stackView.datePicker.date)
            animate(show: false)
        }
    }
}

// MARK: - Custom Classes
class ConfirmButtonContainerView: UIView {
    let button:UIButton = UIButton(frame: .zero)
}

class ParentStackView: UIStackView {
    let datePicker = UIDatePicker(frame: .zero)
    let confirmButtonView = ConfirmButtonContainerView(frame: .zero)
}

extension UIView {
    
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
}

